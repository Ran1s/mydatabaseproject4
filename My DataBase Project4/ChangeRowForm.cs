﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace My_DataBase_Project4
{
    public partial class ChangeRowForm : Form
    {
        Dictionary<string, dynamic> item;
        Dictionary<string, TextBox> textBoxes;
        List<string> nameColumns;
        List<Type> typeColumns;

        public delegate void ItemFunc(Dictionary<string, dynamic> item);
        ItemFunc itemFunc;
        public ChangeRowForm(List<string> nameColumns, List<Type> typeColumns, ItemFunc itemFunc, Dictionary<string, dynamic> item)
        {
            InitializeComponent();       
            textBoxes = new Dictionary<string, TextBox>();
            for (int i = 0; i < nameColumns.Count; i++)
            {
                Label label = new Label();
                label.Text = nameColumns[i];
                label.Top = 5 + i * 30;
                label.Left = 10;

                TextBox textBox = new TextBox();
                textBox.Top = 5 + i * 30;
                textBox.Left = 150;
                textBox.Width = 200;
                if (item.ContainsKey(nameColumns[i]))
                {
                    textBox.Text = item[nameColumns[i]].ToString();
                }
                else
                {
                    textBox.Text = "";
                }
                
                textBoxes.Add(nameColumns[i], textBox);
                

                this.Controls.Add(label);
                this.Controls.Add(textBox);
            }
            this.nameColumns = nameColumns;
            this.typeColumns = typeColumns;
            this.itemFunc = itemFunc;
            this.item = item;
        }

        private void set_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < nameColumns.Count; i++)
            {
                if (typeColumns[i] == Type.GetType("System.Int32"))
                {
                    item[nameColumns[i]] = Int32.Parse(textBoxes[nameColumns[i]].Text);
                }
                else if (typeColumns[i] == Type.GetType("System.String"))
                {
                    item[nameColumns[i]] = textBoxes[nameColumns[i]].Text.ToString();
                }
                else
                {
                    throw new ArgumentException();
                }
            }
            itemFunc(item);
        }
    }
}
