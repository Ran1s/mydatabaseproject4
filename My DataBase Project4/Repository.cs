﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My_DataBase_Project4
{
    class DirectorRepositories
    {
        List<InstanceRepository> repositories = new List<InstanceRepository>();
        public List<InstanceRepository> Repositories { get { return repositories; } }
        public DirectorRepositories()
        {
            List<Table> tables = Storage.LoadTables();
            foreach(Table table in tables)
            {
                repositories.Add(new InstanceRepository(table));
            }
        }
    }
    class InstanceRepository : IRepository
    {
        Table table;
        public Table Table { get { return table; } }

        public InstanceRepository(Table table)
        {
            this.table = table;
        }
        public List<Dictionary<string, dynamic>> GetAll()
        {
            return table.Rows;
        }
        public Dictionary<string, dynamic> Get(int id)
        {
            return table.Rows.First(r => r["id"] == id);
        }
        public Dictionary<string, dynamic> Create(Dictionary<string, dynamic> item)
        {
            int id = table.TableInfo.NextId++;
            item["id"] = id;
            table.Rows.Add(item);
            return item;
        }
        public void Update(Dictionary<string, dynamic> item)
        {
            for(int i = 0; i < table.Rows.Count; i++)
            {
                if(table.Rows[i]["id"] == item["id"])
                {
                    table.Rows[i] = item;
                }
            }
        }
        public void Delete(int id)
        {
            for (int i = 0; i < table.Rows.Count; i++)
            {
                if (table.Rows[i]["id"] == id)
                {
                    table.Rows.RemoveAt(i);
                }
            }
        }
    }
    interface IRepository
    {
        List<Dictionary<string, dynamic>> GetAll();
        Dictionary<string, dynamic> Get(int id);
        Dictionary<string, dynamic> Create(Dictionary<string, dynamic> item);
        void Update(Dictionary<string, dynamic> item);
        void Delete(int id);
    }
}
