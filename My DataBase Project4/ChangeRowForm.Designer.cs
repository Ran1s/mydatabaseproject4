﻿namespace My_DataBase_Project4
{
    partial class ChangeRowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.set = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // set
            // 
            this.set.Location = new System.Drawing.Point(185, 399);
            this.set.Name = "set";
            this.set.Size = new System.Drawing.Size(75, 23);
            this.set.TabIndex = 0;
            this.set.Text = "Отправить";
            this.set.UseVisualStyleBackColor = true;
            this.set.Click += new System.EventHandler(this.set_Click);
            // 
            // ChangeRowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 434);
            this.Controls.Add(this.set);
            this.Name = "ChangeRowForm";
            this.Text = "ChangeRowForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button set;
    }
}