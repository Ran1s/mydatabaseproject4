﻿namespace My_DataBase_Project4
{
    partial class Main
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tables = new System.Windows.Forms.TabControl();
            this.addRow = new System.Windows.Forms.Button();
            this.deleteRow = new System.Windows.Forms.Button();
            this.saveDatabase = new System.Windows.Forms.Button();
            this.changeRow = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tables
            // 
            this.tables.Location = new System.Drawing.Point(12, 95);
            this.tables.Name = "tables";
            this.tables.SelectedIndex = 0;
            this.tables.Size = new System.Drawing.Size(905, 225);
            this.tables.TabIndex = 0;
            this.tables.SelectedIndexChanged += new System.EventHandler(this.tables_SelectedIndexChanged);
            // 
            // addRow
            // 
            this.addRow.Location = new System.Drawing.Point(13, 422);
            this.addRow.Name = "addRow";
            this.addRow.Size = new System.Drawing.Size(75, 23);
            this.addRow.TabIndex = 1;
            this.addRow.Text = "Добавить";
            this.addRow.UseVisualStyleBackColor = true;
            this.addRow.Click += new System.EventHandler(this.addRow_Click);
            // 
            // deleteRow
            // 
            this.deleteRow.Location = new System.Drawing.Point(175, 422);
            this.deleteRow.Name = "deleteRow";
            this.deleteRow.Size = new System.Drawing.Size(75, 23);
            this.deleteRow.TabIndex = 2;
            this.deleteRow.Text = "Удалить";
            this.deleteRow.UseVisualStyleBackColor = true;
            this.deleteRow.Click += new System.EventHandler(this.deleteRow_Click);
            // 
            // saveDatabase
            // 
            this.saveDatabase.Location = new System.Drawing.Point(13, 13);
            this.saveDatabase.Name = "saveDatabase";
            this.saveDatabase.Size = new System.Drawing.Size(75, 23);
            this.saveDatabase.TabIndex = 3;
            this.saveDatabase.Text = "Сохранить";
            this.saveDatabase.UseVisualStyleBackColor = true;
            this.saveDatabase.Click += new System.EventHandler(this.saveDatabase_Click);
            // 
            // changeRow
            // 
            this.changeRow.Location = new System.Drawing.Point(94, 422);
            this.changeRow.Name = "changeRow";
            this.changeRow.Size = new System.Drawing.Size(75, 23);
            this.changeRow.TabIndex = 6;
            this.changeRow.Text = "Изменить";
            this.changeRow.UseVisualStyleBackColor = true;
            this.changeRow.Click += new System.EventHandler(this.changeRow_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(929, 457);
            this.Controls.Add(this.changeRow);
            this.Controls.Add(this.saveDatabase);
            this.Controls.Add(this.deleteRow);
            this.Controls.Add(this.addRow);
            this.Controls.Add(this.tables);
            this.Name = "Main";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tables;
        private System.Windows.Forms.Button addRow;
        private System.Windows.Forms.Button deleteRow;
        private System.Windows.Forms.Button saveDatabase;
        private System.Windows.Forms.Button changeRow;
    }
}

