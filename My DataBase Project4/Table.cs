﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My_DataBase_Project4
{
    class Table
    {
        TableInfo tableInfo;
        List<Dictionary<string, dynamic>> rows;

        public TableInfo TableInfo { get { return tableInfo; } set { tableInfo = value; } }
        public List<Dictionary<string, dynamic>> Rows { get { return rows; } set { rows = value; } }

        public Table(TableInfo tableInfo, List<Dictionary<string, dynamic>> rows)
        {
            this.tableInfo = tableInfo;
            this.rows = rows;
        }
    }
}
