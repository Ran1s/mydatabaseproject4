﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace My_DataBase_Project4
{
    public partial class Main : Form
    {
        DirectorRepositories directorRepositories = new DirectorRepositories();
        List<DataGridView> dataGridViews = new List<DataGridView>();
        int currentTable;
        public Main()
        {
            InitializeComponent();
            foreach(InstanceRepository instanceRepository in directorRepositories.Repositories)
            {
                DataGridView dataGridView = createDataGridView(instanceRepository.Table);
                dataGridView.AllowUserToAddRows = false;
                dataGridView.Width = 1000;
                tables.TabPages.Add(instanceRepository.Table.TableInfo.Name);
                tables.TabPages[tables.TabPages.Count - 1].Controls.Add(dataGridView);
                dataGridViews.Add(dataGridView);
            }
            currentTable = 0;
        }

        private void addRow_Click(object sender, EventArgs e)
        {
            Dictionary<string, dynamic> item = new Dictionary<string,dynamic>();
            ChangeRowForm changeRowForm = new ChangeRowForm(directorRepositories.Repositories[currentTable].Table.TableInfo.NameColumns.ToList<string>(),
                directorRepositories.Repositories[currentTable].Table.TableInfo.TypeColumns.ToList<Type>(), 
                AddRowToTable, item);
            changeRowForm.Owner = this;
            changeRowForm.ShowDialog();
        }

        private void changeRow_Click(object sender, EventArgs e)
        {
            var selectedRow = dataGridViews[currentTable].Rows[dataGridViews[currentTable].SelectedCells[0].RowIndex];
            if(selectedRow != null)
            {
                // Косяк
                Dictionary<string, dynamic> item = directorRepositories.Repositories[currentTable].Get(Int32.Parse(selectedRow.Cells["id"].Value.ToString()));
                ChangeRowForm changeRowForm = new ChangeRowForm(directorRepositories.Repositories[currentTable].Table.TableInfo.NameColumns.ToList<string>(),
                    directorRepositories.Repositories[currentTable].Table.TableInfo.TypeColumns.ToList<Type>(),
                    ChangeRowInTable, item);
                changeRowForm.Owner = this;
                changeRowForm.ShowDialog();
            }
            else
            {
                MessageBox.Show("Не выбрали строку");
            }            
        }

        private void deleteRow_Click(object sender, EventArgs e)
        {
            var selectedRow = dataGridViews[currentTable].Rows[dataGridViews[currentTable].SelectedCells[0].RowIndex];
            if (selectedRow != null)
            {
                directorRepositories.Repositories[currentTable].Delete(Int32.Parse(selectedRow.Cells["id"].Value.ToString()));
                dataGridViews[currentTable].Rows.Remove(selectedRow);
            }
            else
            {
                MessageBox.Show("Не выбрали строку");
            }         

        }

        private void SaveItem_Click(object sender, EventArgs e)
        {

        }

        public void ChangeRowInTable(Dictionary<string, dynamic> item)
        {
            // переделывать в типы item, иначе потом ломается проверка
            directorRepositories.Repositories[currentTable].Update(item);
            string[] r = new string[directorRepositories.Repositories[currentTable].Table.TableInfo.NameColumns.Length];
            for (int i = 0; i < directorRepositories.Repositories[currentTable].Table.TableInfo.NameColumns.Length; i++)
            {
                r[i] = item[directorRepositories.Repositories[currentTable].Table.TableInfo.NameColumns[i]].ToString();
            }
            for (int i = 0; i < dataGridViews[currentTable].Rows.Count; i++)
            {
                if(dataGridViews[currentTable].Rows[i].Cells["id"].Value.ToString() == item["id"].ToString())
                {
                    for (int j = 0; j < r.Length; j++)
                    {
                        dataGridViews[currentTable].Rows[i].Cells[j].Value = r[j];
                    }
                }
            }
        }
        
        public void AddRowToTable(Dictionary<string, dynamic> item)
        {
            item = directorRepositories.Repositories[currentTable].Create(item);
            string[] r = new string[directorRepositories.Repositories[currentTable].Table.TableInfo.NameColumns.Length];
            for (int i = 0; i < directorRepositories.Repositories[currentTable].Table.TableInfo.NameColumns.Length; i++)
            {
                r[i] = item[directorRepositories.Repositories[currentTable].Table.TableInfo.NameColumns[i]].ToString();
            }

            dataGridViews[currentTable].Rows.Add(r);
        }
        DataGridView createDataGridView(Table table)
        {
            DataGridView dataGridView = new DataGridView();
            foreach(string nameColumn in table.TableInfo.NameColumns)
            {
                dataGridView.Columns.Add(nameColumn, nameColumn);
            }
            foreach(Dictionary<string, dynamic> row in table.Rows)
            {
                string[] r = new string[table.TableInfo.NameColumns.Length];
                for(int i = 0; i < table.TableInfo.NameColumns.Length; i++)
                {
                    r[i] = row[table.TableInfo.NameColumns[i]].ToString();
                }
                
                dataGridView.Rows.Add(r);
            }
            return dataGridView;
        }

        private void tables_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentTable = tables.SelectedIndex;
        }

        private void saveDatabase_Click(object sender, EventArgs e)
        {
            foreach(InstanceRepository instanceRepository in directorRepositories.Repositories)
            {
                Storage.Save(instanceRepository.Table);
            }
        }
        
    }
}
