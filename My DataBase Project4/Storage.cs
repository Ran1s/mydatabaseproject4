﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My_DataBase_Project4
{
    class Storage
    {
        public static Table Load(string tableName)
        {
            TableInfo tableInfo = LoadTableInfo(tableName);
            List<Dictionary<string, dynamic>> rows = LoadTableRows(tableName, tableInfo);            
            return new Table(tableInfo, rows);
        }

        public static void Save(Table table)
        {
            SaveTableInfo(table.TableInfo);
            SaveTableRows(table.TableInfo, table.Rows);
        }

        static TableInfo LoadTableInfo(string tableName)
        {
            TableInfo tableInfo = null;
            Dictionary<string, dynamic> info = new Dictionary<string, dynamic>();
            try
            {
                using (StreamReader infoReader = new StreamReader(String.Format(@"Database/{0}/info.txt", tableName)))
                {
                    string line;
                    while ((line = infoReader.ReadLine()) != null)
                    {
                        string[] lineSplit = line.Split(':');
                        if (lineSplit[1].IndexOf(',') > -1)
                        {
                            info[lineSplit[0]] = lineSplit[1].Split(',');
                        }
                        else
                        {
                            info[lineSplit[0]] = lineSplit[1];
                        }
                    }
                }
                //TODO: сделать нормально
                Type[] types = new Type[info["type_columns"].Length];
                for (int i = 0; i < info["type_columns"].Length; i++ )
                {
                    types[i] = Type.GetType(info["type_columns"][i].ToString());
                }
                info["type_columns"] = types;
                tableInfo = new TableInfo(info["name"], Convert.ToInt32(info["count_columns"]), info["type_columns"], info["name_columns"], Convert.ToInt32(info["next_id"]));
            }
            catch (IOException ex)
            {

            }
            return tableInfo;
        }

        Type GetType(string s)
        {
            return Type.GetType(s);
        }

        static List<Dictionary<string, dynamic>> LoadTableRows(string tableName, TableInfo tableInfo)
        {
            List<Dictionary<string, dynamic>> rows = new List<Dictionary<string,dynamic>>();
            try
            {
                using (StreamReader rowsReader = new StreamReader(String.Format("Database/{0}/rows.txt", tableName)))
                {
                    string line;
                    while ((line = rowsReader.ReadLine()) != null)
                    {
                        Dictionary<string, dynamic> row = new Dictionary<string, dynamic>();
                        string[] lineSplit = line.Split('|');
                        for (int i = 0; i < lineSplit.Length; i++)
                        {
                            //TODO: Переделать в тип
                            if (tableInfo.TypeColumns[i] == Type.GetType("System.Int32"))
                            {
                                row[tableInfo.NameColumns[i]] = Int32.Parse(lineSplit[i]);
                            }
                            else if(tableInfo.TypeColumns[i] == Type.GetType("System.String"))
                            {
                                row[tableInfo.NameColumns[i]] = lineSplit[i].ToString();
                            }
                            else
                            {
                                throw new ArgumentException();
                            }
                            
                        }
                        rows.Add(row);
                    }
                }
            }
            catch (IOException ex)
            {

            }
            return rows;
        }

        static void SaveTableInfo(TableInfo tableInfo)
        {
            //TODO: сделать через рефлексию
            try
            {
                using (StreamWriter infoWriter = new StreamWriter(String.Format("Database/{0}/info.txt", tableInfo.Name)))
                {
                    infoWriter.WriteLine(String.Format("count_columns:{0}", tableInfo.CountColumns));
                    infoWriter.WriteLine(String.Format("type_columns:{0}", String.Join(",", tableInfo.TypeColumns.Select(t => t.ToString()))));
                    infoWriter.WriteLine(String.Format("name_columns:{0}", String.Join(",", tableInfo.NameColumns.Select(n => n.ToString()))));
                    infoWriter.WriteLine(String.Format("next_id:{0}", tableInfo.NextId));
                    infoWriter.WriteLine(String.Format("name:{0}", tableInfo.Name));
                }
            }
            catch (IOException ex)
            {

            }       
        }
        static void SaveTableRows(TableInfo tableInfo, List<Dictionary<string, dynamic>> rows)
        {
            try
            {
                using(StreamWriter rowsWriter = new StreamWriter(String.Format("Database/{0}/rows.txt", tableInfo.Name)))
                {
                    foreach(Dictionary<string, dynamic> row in rows)
                    {
                        string line = "";
                        for (int i = 0; i < tableInfo.CountColumns; i++)
                        {
                            line += row[tableInfo.NameColumns[i]].ToString();
                            if(i != tableInfo.CountColumns - 1)
                            {
                                line += "|";
                            }
                        }
                        rowsWriter.WriteLine(line);
                    }
                }
            }
            catch(IOException ex)
            {

            }            
        }

        static public List<Table> LoadTables()
        {
            List<Table> tables = new List<Table>();
            try
            {
                using (StreamReader tablesReader = new StreamReader(@"Database/tables.txt"))
                {
                    string line;
                    while((line = tablesReader.ReadLine()) != null)
                    {
                        tables.Add(Load(line));
                    }
                }
            }
            catch(IOException ex)
            {

            }
            return tables;
        }
        static public void SaveTables(List<Table> tables)
        {
            try
            {
                using(StreamWriter tablesWriter = new StreamWriter(@"Database/tables.txt"))
                {
                    foreach(Table table in tables)
                    {
                        tablesWriter.WriteLine(table.TableInfo.Name);
                        Save(table);
                    }
                }
            }
            catch(IOException ex)
            {

            }
        }
    }
}
