﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My_DataBase_Project4
{
    class TableInfo
    {
        int countColumns;
        Type[] typeColumns;
        string[] nameColumns;
        int nextId;
        string name;
        int countRows;
        public int CountColumns { get { return countColumns; } }
        public Type[] TypeColumns { get { return typeColumns; } }
        public string[] NameColumns { get { return nameColumns; } }
        public int NextId { get { return nextId; } set { nextId = value; } }
        public string Name { get { return name; } }
        
        public TableInfo(string name, int countColumns, Type[] typeColumns, string[] nameColumns, int nextId)
        {
            this.name = name;
            this.countColumns = countColumns;
            this.typeColumns = typeColumns;
            this.nameColumns = nameColumns;
            this.nextId = nextId;
        }
    }
}
